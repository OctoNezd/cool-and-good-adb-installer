import ctypes
import sys
import os
import tempfile
from io import BytesIO
from zipfile import ZipFile
import platform
import requests
PLATFORM_TOOLS_URL = "https://dl.google.com/android/repository/platform-tools-latest-windows.zip"
USB_DRIVER_URL = "https://dl-ssl.google.com/android/repository/latest_usb_driver_windows.zip"
__version__ = "1.0"

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False


def install_driver(url):
    print("  Downloading USB driver...")
    r = requests.get(url)
    zip = ZipFile(BytesIO(r.content), 'r')
    print("  Unpacking USB driver...")
    tempdir = tempfile.gettempdir()
    zip.extractall(os.path.join(tempdir, "adbusb"))
    print("  Installing USB driver...")
    if '32bit' in platform.architecture():
        pnputil = '%systemroot%\\Sysnative\\pnputil.exe'
    else:
        pnputil = 'pnputil.exe'
    install_str = " ".join([pnputil, "-i", "-a",
                            os.path.join(tempdir, "adbusb", "usb_driver", "android_winusb.inf")])
    os.system(install_str)


def install_ptools(url):
    print("  Downloading platform tools...")
    r = requests.get(url)
    zip = ZipFile(BytesIO(r.content), 'r')
    print("  Unpacking platform tools...")
    zip.extractall("C:\\")
    print("  Installing platform tools into %PATH%...")
    os.system('setx /m PATH "%PATH%;C:\\platform-tools\\"')


if __name__ == '__main__':

    if is_admin():
        print("# Cool and good adb installer.")
        print("# By OctoNezd")
        try:
            user = input(
                "Do you want to install adb, fastboot and Android USB driver?[Y/N]")
            assert user.lower() == "y"

            user = input(
                "Did you read the https://developer.android.com/studio/terms?[Y/N]")
            assert user.lower() == "y"

            print("Platform tools(adb, fastboot):")
            install_ptools(PLATFORM_TOOLS_URL)
            print("USB Driver:")
            install_driver(USB_DRIVER_URL)
            input("Install complete! Press Enter to exit!")
        except AssertionError:
            print("! Installation canceled")
            input("Press enter to exit...")
            raise SystemExit
    else:
        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(
            None, "runas", sys.executable, __file__, None, 1)
